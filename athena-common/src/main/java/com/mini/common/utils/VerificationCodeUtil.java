package com.mini.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @ClassName : VerificationCodeUtil
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/10/16  16:48
 */
@Component
public class VerificationCodeUtil {

    private static Logger _logger = LoggerFactory.getLogger(VerificationCodeUtil.class);

    private static int width;  //图片的宽和高

    @Value("${image.width}")
    public void setWidth(int width) {
        VerificationCodeUtil.width = width;
    }

    private static int height;

    @Value("${image.height}")
    public void setHeight(int height) {
        VerificationCodeUtil.height = height;
    }

    private static int length;  //字符长度

    @Value("${image.length}")
    public void setLength(int length) {
        VerificationCodeUtil.length = length;
    }

    private static int randomLineCount;  //干扰线数量

    @Value("${image.randomLineCount}")
    public void setRandomLineCount(int randomLineCount) {
        VerificationCodeUtil.randomLineCount = randomLineCount;
    }

    private static int randomPointCount;  //干扰电数量

    @Value("${image.randomPointCount}")
    public void setRandomPointCount(int randomPointCount) {
        VerificationCodeUtil.randomPointCount = randomPointCount;
    }

    private static StringBuffer text = new StringBuffer();  //验证码内容
    private static Random random = new Random();  //随机数对象
    private static final double factor = 0.75;// 计算字体大小
    private static final String[] fontNames = new String[]{"宋体", "华文楷体", "黑体", "微软雅黑", "楷体_GB2312"};
    private static final String codes = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";


    public static BufferedImage getImage() {
        return getImage(width, height);
    }

    public static BufferedImage getImage(int width, int height) {
        return getImage(width, height, length);
    }

    public static BufferedImage getImage(int width, int height, int length) {
        return getImage(width, height, length, randomLineCount, randomPointCount);
    }

    public static BufferedImage getImage(int width, int height, int length, int randomLineCount,
                                         int randomPointCount) {
        BufferedImage image = null;
        synchronized (VerificationCodeUtil.class) {
            try {
                //创建图片缓冲区
                image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                //获取画笔
                Graphics g = image.getGraphics();
                //设置背景
                setBackGround(g, width, height);
                //画线
                drawLine(g, width, height, randomLineCount);
                //画点
                drawPoint(g, width, height, randomPointCount);
                //写随机字符
                drawRandomChar((Graphics2D) g, width, height, length);
                g.dispose();
            } catch (Exception e) {
                _logger.error(e.getMessage());
            }
        }
        return image;
    }

    public static String getValidateCode() {
        return text.toString();
    }


    //获取字体随机颜色
    private static Color randomColor() {
        //这里为什么是150，因为当r，g，b都为255时，即为白色，为了好辨认，需要颜色深一点。
        int r = random.nextInt(150);
        int g = random.nextInt(150);
        int b = random.nextInt(150);
        return new Color(r, g, b);
    }

    //获取随机字体
    private static Font randomFont(int height) {
        //获取随机字体
        int index = random.nextInt(fontNames.length);
        String fontName = fontNames[index];
        //随机字体大小
        int size = random.nextInt(5) + (int) (height * factor);
        //随机获取字体的样式，0是无样式，1是加粗，2是斜体，3是加粗加斜体

        int style = random.nextInt(4);
        return new Font(fontName, style, size);
    }

    //获取随机字符
    private static char randomChar() {
        int index = random.nextInt(codes.length());
        return codes.charAt(index);
    }

    private static void drawRandomChar(Graphics2D g, int w, int h, int l) {
        text.delete(0, text.length());//清空之前的验证码
        int x = 3; //和坐标位置
        int y = 0; //纵坐标位置
        int degree = 0;//字符旋转角度
        char n;
        for (int i = 0; i < length; i++) {
            g.setFont(randomFont(h));//设置字体
            g.setColor(randomColor()); //字体颜色
            degree = random.nextInt(60) - 30;// 设置旋转角度
            y = random.nextInt(5) + (int) (height * factor); //设置纵坐标的位置
            g.rotate((degree * Math.PI) / 180, x, y); //旋转角度
            n = randomChar();
            g.drawString(String.valueOf(n), x, y);
            g.rotate(-(degree * Math.PI) / 180, x, y); //还原角度
            x = x + width / length;
            //记录验证码
            text.append(n);
        }
    }

    //画干扰线
    private static void drawLine(Graphics g, int w, int h, int randomLineCount) {
        if (randomLineCount <= 0) {
            randomLineCount = 10;
        }
        //画线
        int x1, y1, x2, y2 = 0;
        for (int i = 0; i < randomLineCount; i++) {
            x1 = random.nextInt(w);
            y1 = random.nextInt(h);
            x2 = random.nextInt(w);
            y2 = random.nextInt(h);
            g.setColor(getRandColor(0, 255));
            g.drawLine(x1, y1, x2, y2);
        }

    }

    //画点
    private static void drawPoint(Graphics g, int w, int h, int randomPointCount) {
        if (randomPointCount >= 0) {
            randomPointCount = 80;
        }
        //画点
        int x, y = 0;
        for (int i = 0; i < randomPointCount; i++) {
            x = random.nextInt(w);
            y = random.nextInt(h);
            g.setColor(getRandColor(0, 255));
            g.drawLine(x, y, x, y);
        }
    }

    //创建图片
    private static void setBackGround(Graphics g, int width, int height) {
        //设置背景色
        g.setColor(getRandColor(200, 255));
        g.fillRect(0, 0, width, height);
    }

    //获取背景颜色和划线颜色
    private static Color getRandColor(int fc, int bc) {
        if (fc > 255) {
            fc = 255;
        }
        if (bc > 255) {
            bc = 255;
        }
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }


    public static Map<String,String> getBase64Image4Code() throws IOException {
        Map<String,String> map = new HashMap<>();
        BufferedImage image = getImage();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image,"JPEG",outputStream);
        byte[] bytes = outputStream.toByteArray();
        String base64Image = Base64.getEncoder().encodeToString(bytes);
        map.put("code",getValidateCode());
        map.put("image",base64Image);
        return map;
    }

}
