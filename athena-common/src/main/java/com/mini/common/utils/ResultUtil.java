package com.mini.common.utils;

import com.mini.common.domain.Response;
import com.mini.common.enums.ResultEnum;

/**
 * @ClassName : ResultUtil
 * @Description : 响应数据工具类
 * @Author : Guoxuming
 * @Date : 2020/9/18  16:54
 */
public class ResultUtil {

    /*
      * @Description :
      * @Param :
      * @Return : 默认返回成功编码
      * @Author : guoxuming
      * @Date : 2020/9/18 17:03
      */
    public static Response success(){
        return success(ResultEnum.SUCCESS);
    }
    public static <T>Response success(T data){
        return success(ResultEnum.SUCCESS,data);
    }

    public static Response success(ResultEnum resultEnum) {
        return success(resultEnum,null);
    }

    public static <T> Response success(ResultEnum resultEnum,T data) {

        return success(resultEnum.code(),data,resultEnum.message());
    }
    public static <T> Response success(int code,T data,String message) {

        return new Response<T>(code,data,message);
    }

    public static Response failure(){
        return failure(ResultEnum.FAILURE);
    }
    public static Response failure(String message){
        return failure(ResultEnum.FAILURE.code(),message);
    }

    public static Response failure(ResultEnum resultEnum) {
        return failure(resultEnum.code(),resultEnum.message());
    }

    public static  Response failure(int code,String message) {

        return new Response<>(code,null,message);
    }


}
