package com.mini.common.utils;

import cn.hutool.core.bean.BeanUtil;
import com.mini.common.domain.Dict;
import com.mini.common.domain.DictItems;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName : TransUtil
 * @Description : 数据字典翻译
 * @Author : Guoxuming
 * @Date : 2020/7/10  15:02
 */
public class TransUtil {

    private static Logger _logger = LoggerFactory.getLogger(TransUtil.class);
    //新增加一个翻译结果字段
    public static List<Map<String,Object>> trans4AddCodeName(List<Map<String ,Object>> dataList, List<DictItems> dictList){
        return trans(dataList,dictList,true);
    }
    //替换code值为value
    public static  List<Map<String,Object>> trans4ReplaceCode(List<Map<String ,Object>> dataList, List<DictItems> dictList){
        return trans(dataList,dictList,false );
    }

    private static  List<Map<String,Object>> trans(List<Map<String ,Object>> dataList, List<DictItems> dictList,boolean flag){
        _logger.info("翻译数据量{},需要翻译的字段数{}",dataList.size(),dictList.size());
        //遍历数据结果
        for (Map<String, Object> data : dataList) {
        //    遍历需要翻译的字段
            for (DictItems dictItems : dictList) {
            //    获取数据库中存放的字段名称
                String codeName = dictItems.getName();
                //如果数据结果集中不存在 该翻译的字段就跳过
                if(StringUtils.isEmpty(data.get(codeName))){
                    continue;
                }
            //    获取该翻译字段所有的字典值
                List<Dict> dicts = dictItems.getDicts();
                //遍历所有字典值
                if (dicts.size() > 0) {
                    for (Dict dict : dicts) {
                        if(dict.getCodeValue().equals(data.get(codeName).toString())){
                            if(flag){
                                data.put(codeName+"Name",dict.getCodeName());
                            }else {
                                data.put(codeName,dict.getCodeName());
                            }
                        }
                    }
                }
            }
        }
        return dataList;
    }
    //工具类 集合转换map
    public static<T> List<Map<String,Object>> bean2Map(List<T> list){
        List<Map<String,Object>> listMap = new ArrayList<>();
        for (T t : list) {
            Map<String, Object> map = BeanUtil.beanToMap(t);
            listMap.add(map);
        }
        return listMap;
    }
//    基本原理
    public static<T> List<Map<String,Object>> beanToMap(List<T> list) throws Exception{
        List<Map<String,Object>> listMap = new ArrayList<>();
        for (T t : list) {
            Map<String,Object> map =new HashMap<>();
            BeanInfo beanInfo = Introspector.getBeanInfo(t.getClass());
            //属性描述数组
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor descriptor : propertyDescriptors) {
                //属性名称
                String name = descriptor.getName();
            //    获取属性值的get方法名
                Method getMethod = descriptor.getReadMethod();
                Object value = getMethod.invoke(t);
                map.put(name,value);
            }
            listMap.add(map);
        }
        return listMap;
    }
    //反射方式翻译标准代码    替换原code值
    public synchronized static<T> List<T> transBean(List<T> dataList,List<DictItems> dictList){
        dataList.forEach(data ->{
            Field[] fields = data.getClass().getDeclaredFields();
            for (Field field : fields) {
                dictList.forEach(dic->{
                    if(field.getName().equals(dic.getName())){
                        field.setAccessible(true);
                        dic.getDicts().forEach(d->{
                            try {
                                if(field.get(data).equals(d.getCodeName())){
                                    field.set(data,d.getCodeValue());
                                }
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                });
            }
        });
        return dataList;
    }
}
