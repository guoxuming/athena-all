package com.mini.common.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @ClassName : Dict
 * @Description :数据字典对象信息
 * @Author : Guoxuming
 * @Date : 2020/7/10  16:33
 */
@ApiModel("数据字典对象信息 ")
public class Dict {
    @ApiModelProperty("标准代码值")
    private String codeValue;
    @ApiModelProperty("标准代码名称")
    private String codeName;

    public String getCodeValue() {
        return codeValue;
    }

    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    @Override
    public String toString() {
        return "Dict{" +
                "codeValue='" + codeValue + '\'' +
                ", codeName='" + codeName + '\'' +
                '}';
    }
}
