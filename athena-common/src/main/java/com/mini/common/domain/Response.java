package com.mini.common.domain;

/**
 * @ClassName : Response
 * @Description : 返回数据模型
 * @Author : Guoxuming
 * @Date : 2020/9/18  16:42
 */
public class Response<T> {

    //响应编码
    private int code;
    //响应数据
    private T data;
    //响应信息
    private String message;


    public Response() {
    }

    public Response(int code, T data, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Response{" +
                "code=" + code +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }
}
