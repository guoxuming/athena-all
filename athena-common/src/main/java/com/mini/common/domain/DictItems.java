package com.mini.common.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @ClassName : DictItems
 * @Description :数据字典信息及对应的字段值
 * @Author : Guoxuming
 * @Date : 2020/7/10  16:32
 */
@ApiModel("数据字典信息及对应的字段值")
public class DictItems {
    @ApiModelProperty("表中字段值")
    private String name;
    @ApiModelProperty("数据字典信息")
    private List<Dict> dicts;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Dict> getDicts() {
        return dicts;
    }

    public void setDicts(List<Dict> dicts) {
        this.dicts = dicts;
    }

    @Override
    public String toString() {
        return "DictItems{" +
                "name='" + name + '\'' +
                ", dicts=" + dicts +
                '}';
    }
}
