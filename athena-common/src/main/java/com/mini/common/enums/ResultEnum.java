package com.mini.common.enums;

/**
 * @ClassName : ResultEnum
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/18  15:37
 */
public enum ResultEnum {
    /* 成功状态码 */
    SUCCESS(200, "成功"),
    /* 失败状态码 */
    FAILURE(400, "失败"),

    /* 参数错误：10001-19999 */
    USER_HAS_EXISTED(20005, "用户已存在"),

    /* 业务错误：30001-39999 */

    /* 系统错误：40001-49999 */
    SYSTEM_INNER_ERROR(40001, "系统繁忙，请稍后重试"),

    /* 数据错误：50001-599999 */
    RESULE_DATA_NONE(50001, "数据未找到"),
    DATA_IS_WRONG(50002, "数据有误"),
    DATA_ALREADY_EXISTED(50003, "数据已存在"),

    /* 接口错误：60001-69999 */
    INTERFACE_INNER_INVOKE_ERROR(60001, "内部系统接口调用异常"),
    INTERFACE_OUTTER_INVOKE_ERROR(60002, "外部系统接口调用异常"),
    INTERFACE_FORBID_VISIT(60003, "该接口禁止访问"),
    INTERFACE_ADDRESS_INVALID(60004, "接口地址无效"),
    INTERFACE_REQUEST_TIMEOUT(60005, "接口请求超时"),
    INTERFACE_EXCEED_LOAD(60006, "接口负载过高"),

    /* 权限错误：70001-79999 */
    PERMISSION_NO_ACCESS(70001, "无访问权限");

    private Integer code;
    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    public Integer code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }

    public Integer getCode(String message) {
        ResultEnum[] values = values();
        for (ResultEnum value : values) {
            if(value.message.equals(message)){
                return value.code;
            }
        }
        return code;
    }

    public String getMessage(int code) {
        ResultEnum[] values = values();
        for (ResultEnum value : values) {
            if(code == value.code){
                return value.message;
            }
        }
        return message;
    }

    @Override
    public String toString() {
        return "ResultEnum{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
