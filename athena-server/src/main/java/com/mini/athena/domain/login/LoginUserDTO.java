package com.mini.athena.domain.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @ClassName : LoginUserDTO
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/17  16:06
 */
@Data
@ToString
@Builder
@ApiModel("登录信息")
public class LoginUserDTO {

    @ApiModelProperty("账号")
    private String username;
    @ApiModelProperty("密码")
    private String password;

}
