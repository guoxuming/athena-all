package com.mini.athena.LoginConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @ClassName : LoginPageConfig
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/2  17:23
 */
@Controller
public class LoginPageConfig {

    @RequestMapping("/")
    public ModelAndView loginPage(){
        return new ModelAndView("login.html");
    }
}
