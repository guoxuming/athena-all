package com.mini.athena.api.controller.login;

import com.mini.athena.domain.LoginUser;
import com.mini.athena.domain.Token;
import com.mini.athena.domain.login.LoginUserDTO;
import com.mini.athena.service.JwtTokenService;
import com.mini.athena.utils.UUIDAndDateutil;
import com.mini.common.domain.Response;
import com.mini.common.utils.ResultUtil;
import com.mini.common.utils.VerificationCodeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName : LoginController
 * @Description : 用户登录
 * @Author : Guoxuming
 * @Date : 2020/9/14  15:48
 */
@RestController
@Api(description = "用户操作",tags = {"用户操作"})
//@RequestMapping("/login")
public class LoginController {

    private Logger _logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private JwtTokenService jwtTokenService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RedisTemplate redisTemplate;

    @PostMapping("/login")
    @ApiOperation("账号登录")
    public Response login(@RequestBody LoginUserDTO loginUserDTO){
        //SM4Utils sm4Utils = new SM4Utils();  //国密算法
        _logger.info("登录用户信息:{}",loginUserDTO);
        String username = loginUserDTO.getUsername();
        //添加认证账户信息
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, loginUserDTO.getPassword());
        Authentication authenticate = null;
        try {
            //security内部认证
            authenticate = authenticationManager.authenticate(authenticationToken);
        } catch (AuthenticationException e) {
            _logger.error("认证失败,失败原因:{}",e.toString());
            //添加其他业务逻辑
            return ResultUtil.failure(e.toString());
        }
        //认证结果放入线程中
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        //根据账户获取用户信息
        LoginUser loginUser = (LoginUser)userDetailsService.loadUserByUsername(username);
        _logger.info("当前用户信息:{}",loginUser);
        Token token = jwtTokenService.saveToken(loginUser);
        return ResultUtil.success(token);

    }
    @GetMapping("/getCurrentUser")
    @ApiOperation("获取当前登录用户信息")
    public Response getCurrentUser(HttpServletRequest req){
        String jwtToken = req.getHeader("token");
        //根据令牌获取当前登录用户
        LoginUser userInfo = jwtTokenService.getUserInfo(jwtToken);
        return ResultUtil.success(userInfo);
    }

    @GetMapping("/getCurrentUsername")
    @ApiOperation("获取当前登录人")
    public Response getCurrentUsername(HttpServletRequest req){

        //Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        //Object principal = authentication.getPrincipal();
        //_logger.info("当前登录用户{}",principal);
        String jwtToken = req.getHeader("token");
        //根据令牌获取当前登录用户
        String username = jwtTokenService.getUsername(jwtToken);
        return ResultUtil.success(username);

    }

    @GetMapping("/logout")
    @ApiOperation("登出操作")
    public Response logout(HttpServletRequest req){
        String jwtToken = req.getHeader("token");
        //根据令牌获取当前登录用户
        if(jwtTokenService.delToken(jwtToken)){
            return ResultUtil.success();
        }
        return ResultUtil.failure();

    }

    @GetMapping("/verificationCode")
    @ApiOperation("获取验证码")
    public Response verificationCode(@RequestParam(required = false) String id) throws IOException {
        Map<String,String> map = new HashMap<>();
        //获取图片
        BufferedImage image = VerificationCodeUtil.getImage();
        String validateCode = VerificationCodeUtil.getValidateCode();
        String uuid = UUIDAndDateutil.getUUID();
        redisTemplate.boundValueOps(uuid).set(validateCode,10, TimeUnit.MINUTES);
        //输出流
        _logger.info("codeId:{},code:{}",uuid,validateCode);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image,"JPEG",outputStream);
        byte[] bytes = outputStream.toByteArray();
        String picture = Base64.getEncoder().encodeToString(bytes);
        map.put("codeId",uuid);
        map.put("picture",picture);
        return ResultUtil.success(map);
    }

    @GetMapping("/checkCode/{code}/{codeId}")
    @ApiOperation("验证验证码")
    public Response checkCode(@PathVariable("code")String code, @PathVariable("codeId")String codeId){
        if(StringUtils.isBlank(code) && StringUtils.isBlank(codeId)){
            return ResultUtil.failure("验证码不能为空!");
        }else if(!redisTemplate.hasKey(codeId)){
            return ResultUtil.failure("验证码错误!");
        } else if(code.equalsIgnoreCase((String) redisTemplate.opsForValue().get(codeId))){
            return ResultUtil.success();
        }
        return ResultUtil.failure("验证码错误!");
    }

    private boolean checkFormCode(String code, String codeId){
        if(StringUtils.isBlank(code) && StringUtils.isBlank(codeId)){
            throw new IllegalArgumentException("验证码不能为空!");
        }else if(!redisTemplate.hasKey(codeId)){
            throw new IllegalArgumentException("验证码错误!");
        } else if(code.equalsIgnoreCase((String) redisTemplate.opsForValue().get(codeId))){
            return true;
        }
        return false;
    }

}
