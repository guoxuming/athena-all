package com.mini.athena.api.impl;

import com.mini.athena.VerificationCodeApi;
import com.mini.common.utils.VerificationCodeUtil;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

/**
 * @ClassName : VerificationCodeApiImpl
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/11/9  17:16
 */
@RestController
public class VerificationCodeApiImpl implements VerificationCodeApi {

    @Override
    public Map<String, String> getCodeOfBase64() throws IOException {
        return VerificationCodeUtil.getBase64Image4Code();
    }
}
