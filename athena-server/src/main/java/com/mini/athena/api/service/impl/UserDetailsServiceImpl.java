package com.mini.athena.api.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.mini.athena.api.service.UserService;
import com.mini.athena.domain.LoginUser;
import com.mini.athena.domain.PermissionInfo;
import com.mini.athena.domain.UserInfo;
import com.mini.athena.enums.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName : UserDetailsServiceImpl
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/3  15:47
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfo userInfo = userService.getUserByUserAccount(username);
        if(userInfo == null){
            throw new UsernameNotFoundException("username"+username+"is not exist!");
        }else if(UserStatus.LOCKED.getStatus().equals(userInfo.getAccountStatus())){
            throw  new LockedException("账户被锁定,请联系管理员");
        }else if(UserStatus.DISABLE.getStatus().equals(userInfo.getAccountStatus())){
            throw  new DisabledException("账户已禁用!");
        }
        //登录用户信息装配
        LoginUser loginUser = new LoginUser();
        BeanUtil.copyProperties(userInfo,loginUser);
        //根据用户标识查询权限
        List<PermissionInfo> permissions = userService.getPermissionById(userInfo.getId());
        loginUser.setPermissionInfos(permissions);
        return loginUser;
    }
}
