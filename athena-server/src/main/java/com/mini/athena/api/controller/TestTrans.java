package com.mini.athena.api.controller;

import com.mini.athena.api.service.TestTransService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName : TestTrans
 * @Description :测试事务
 * @Author : Guoxuming
 * @Date : 2020/9/18  17:54
 */
@RestController
@RequestMapping("/test")
@Api("测试事务")
public class TestTrans {
    @Autowired
    private TestTransService testTransService;

    @GetMapping("/trans")
    public void setName(){
        testTransService.setName("张三");
    }
}
