package com.mini.athena.api.service.impl;

import com.mini.athena.api.service.TestTransService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @ClassName : TestTransServiceImpl
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/18  17:55
 */
@Service
public class TestTransServiceImpl implements TestTransService {

    @Override
    @Transactional
    public void setName(String name) {

    }
}
