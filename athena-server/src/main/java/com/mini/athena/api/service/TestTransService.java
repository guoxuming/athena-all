package com.mini.athena.api.service;

/**
 * @ClassName : TestTransService
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/18  17:55
 */
public interface TestTransService {
    void setName(String name);
}
