package com.mini.athena.api.service;

import com.mini.athena.domain.PermissionInfo;
import com.mini.athena.domain.UserInfo;

import java.util.List;

/**
 * @ClassName : UserService
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/3  15:49
 */
public interface UserService {

    UserInfo getUserByUserAccount(String account);

    List<PermissionInfo> getPermissionById(int userId);
    //账号注册
    void register(UserInfo userInfo);
    //修改用户信息
    void updateUser(UserInfo userInfo);
    //修改密码
    void updatePassword(String username,String oldPassword,String newPassword);
    //账号注销
    void cancel(String username);

}
