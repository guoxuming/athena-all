package com.mini.athena.api.controller;

import com.mini.common.domain.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName : RoleController
 * @Description : 角色添加
 * @Author : Guoxuming
 * @Date : 2020/9/22  10:57
 */
@Api(description = "角色添加",tags = {"角色添加"})
@RestController
@RequestMapping("/role")
public class RoleController {
    private Logger _logger = LoggerFactory.getLogger(RoleController.class);


    @PostMapping("/addRole")
    @ApiOperation("添加角色")
    public Response addRole(){
        return null;
    }

    @DeleteMapping("/del")
    @ApiOperation("删除角色")
    public Response delRole(){

        return null;
    }


}
