package com.mini.athena.api.controller;

import com.mini.athena.api.service.UserService;
import com.mini.athena.domain.UserInfo;
import com.mini.athena.service.JwtTokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName : UserController
 * @Description : 用户操作
 * @Author : Guoxuming
 * @Date : 2020/9/29  15:41
 */
@RestController
@RequestMapping("/user")
@Api(description = "用户信息操作",tags = {"用户信息操作"})
public class UserController {
    private Logger _logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private JwtTokenService jwtTokenService;
    @Autowired
    private UserService userService;

    @PostMapping("/register")
    @ApiOperation("账号注册")
    public void register(@RequestBody UserInfo userInfo){
        _logger.info("注册用户信息{}",userInfo);
        if(StringUtils.isBlank(userInfo.getUsername()) ||StringUtils.isBlank(userInfo.getPassword())){
            throw new IllegalArgumentException("注册账号密码不能为空!");
        }
        userService.register(userInfo);
    }

    @GetMapping("/cancel")
    @ApiOperation("账号注销")
    public void cancel(HttpServletRequest request){
        String jwtToken = request.getHeader("token");
        String username = jwtTokenService.getUsername(jwtToken);
        userService.cancel(username);
    }


    @PostMapping("/updateUser")
    @ApiOperation("修改用户信息")
    public void updateUser(@RequestBody UserInfo userInfo){
        _logger.info("注册用户信息{}",userInfo);
        userService.updateUser(userInfo);
    }

    @PostMapping("/updatePassword")
    @ApiOperation("修改密码")
    public void updatePassword(@RequestBody UserInfo userInfo){
        _logger.info("修改密码{}",userInfo);
        //userService.updatePassword();
    }

}
