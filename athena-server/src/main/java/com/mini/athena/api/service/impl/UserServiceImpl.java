package com.mini.athena.api.service.impl;

import com.mini.athena.api.service.UserService;
import com.mini.athena.domain.PermissionInfo;
import com.mini.athena.domain.UserInfo;
import com.mini.athena.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ClassName : UserServiceImpl
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/3  16:36
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserInfo getUserByUserAccount(String account) {
        return userMapper.getUserByUserAccount(account);
    }

    @Override //根据用户标识查询资源权限
    public List<PermissionInfo> getPermissionById(int userId) {

        return userMapper.getPermissionById(userId);
    }

    @Override
    public void register(UserInfo userInfo) {
        userMapper.register(userInfo);
    }

    @Override
    @Transactional
    public void updateUser(UserInfo userInfo) {
        userMapper.updateUser(userInfo);
    }

    @Override
    public void updatePassword(String username, String oldPassword, String newPassword) {
        UserInfo userInfo = userMapper.getUserByUserAccount(username);
        if(userInfo ==null){
            throw new IllegalArgumentException("用户不存在!");
        }
        if(!passwordEncoder.matches(oldPassword,userInfo.getPassword())){
            throw new IllegalArgumentException("旧密码错误!");
        }
        userMapper.updatePassword(userInfo.getId(),passwordEncoder.encode(newPassword));

    }

    @Override
    public void cancel(String username) {
        userMapper.cancel(username);
    }
}
