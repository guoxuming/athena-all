package com.mini.athena;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName : AthenaApplication
 * @Description : 启动类
 * @Author : Guoxuming
 * @Date : 2020/8/28  16:24
 */
@ComponentScan(basePackages = {"com.mini.common.**","com.mini.athena.**"})
@SpringBootApplication
@EnableFeignClients(basePackages = {""})
@EnableDiscoveryClient
//@RefreshScope  //开始动态刷新配置功能
public class AthenaApplication {
    public static void main(String[] args) {
        
        SpringApplication.run(AthenaApplication.class,args);
        System.out.println("项目启动成功!");
    }
}
