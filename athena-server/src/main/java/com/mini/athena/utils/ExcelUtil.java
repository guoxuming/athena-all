package com.mini.athena.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName : ExcelUtil
 * @Description : Excel导入导出
 * @Author : Guoxuming
 * @Date : 2020/7/10  14:48
 */
public class ExcelUtil {
    private static Logger _logger = LoggerFactory.getLogger(ExcelUtil.class);
    private final static String XLS = "xls";
    private final static String XLSX = "xlsx";

    //下载到浏览器
    public static void excelExport(String fileName, List<String> headList, List<Map<String, Object>> resultList, String[] rowName,
                                   HttpServletResponse response) {
        Workbook workbook = getWorkbook(headList, resultList, rowName);
        if(workbook!=null){
            write2Browser(response,workbook,fileName);
        }
    }

    //下载到指定位置
    public static void excelExport(String path, String fileName, List<String> headList, List<Map<String, Object>> resultList, String[] rowName) {
        Workbook workbook = getWorkbook(headList, resultList, rowName);
        if(workbook!=null){
            write2Path(path,workbook,fileName);
        }
    }

    private static Workbook getWorkbook(List<String> headList, List<Map<String, Object>> resultList ,String[] rowName) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet");
        Row row = null;
        Cell cell = null;
        XSSFCellStyle style = workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER_SELECTION);//居中显示

        Font font = workbook.createFont();
        int line = 0, maxColumn = 0; //行数    列数
        if (headList != null && headList.size() > 0) { //设置列头
            //创建行
            row = sheet.createRow(line++);
            row.setHeightInPoints(20);//设置行高
            //设置字体加粗和字体大小
            font.setBold(true);//加粗
            font.setFontHeightInPoints((short) 15);  //默认11号字体
            style.setFont(font);
            maxColumn = headList.size();
            for (int i = 0; i < maxColumn; i++) {
                cell=row.createCell(i);
                cell.setCellStyle(style);
                cell.setCellValue(headList.get(i));
            }
        }
        if(resultList !=null && resultList.size()>0){//渲染数据
            for (Map<String, Object> map : resultList) {
                if (map !=null) {
                    row = sheet.createRow(line++);
                    row.setHeightInPoints(16);
                    for (int i = 0; i < rowName.length; i++) {
                        cell = row.createCell(i);//此处使用单元格默认样式
                        cell.setCellValue(map.get(rowName[i]) ==null? "":map.get(rowName[i]).toString());
                    }
                }
            }
        }

        return workbook;
    }

    @Deprecated
    public static void exportExcel(HttpServletResponse response, List<String> headList, List<Map<String, Object>> resultList, String[] rowName, String fileName) {
        if (resultList.size() <= 0) {
            throw new RuntimeException("没有导出的数据!");
        }
        _logger.info("导出的数据{}", resultList);
        //创建excel表格对象  07版
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet");
        //设置列默认宽度
        sheet.setDefaultColumnWidth(20);
        //创建单元格样式
        XSSFCellStyle headCellStyle = workbook.createCellStyle();
        //水平居中展示
        headCellStyle.setAlignment(HorizontalAlignment.CENTER);
        //垂直居中
        headCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        XSSFFont headFont = workbook.createFont();
        //字体大小
        headFont.setFontHeightInPoints((short) 18);
        //是否加粗  默认false
        headFont.setBold(true);
        headCellStyle.setFont(headFont);
        //写入表头
        XSSFRow headRow = sheet.createRow(0);
        XSSFCell headCell;
        for (int i = 0; i < headList.size(); i++) {
            headCell = headRow.createCell(i);
            //写入值
            headCell.setCellValue(headList.get(i));
            //写入样式
            headCell.setCellStyle(headCellStyle);
        }
        //写入数据
        //创建单元格样式
        XSSFCellStyle cellStyle = workbook.createCellStyle();
        //水平居中展示
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        //垂直居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        XSSFFont font = workbook.createFont();
        //字体大小
        font.setFontHeightInPoints((short) 16);
        //是否加粗  默认false
        //font.setBold(false);
        cellStyle.setFont(font);
        int i = 1;
        for (Map<String, Object> map : resultList) {
            XSSFRow bodyRow = sheet.createRow(i);
            XSSFCell cell = null;
            for (int j = 0; j < rowName.length; j++) {
                cell = bodyRow.createCell(j);
                cell.setCellStyle(cellStyle);
                Object valueObject = map.get(rowName[j]);
                String value = null;
                if (valueObject == null) {
                    valueObject = "";
                }
                if (valueObject instanceof String) {
                    value = (String) valueObject;
                } else if (valueObject instanceof BigDecimal) {
                    value = String.valueOf(valueObject);
                } else {
                    value = valueObject.toString();
                }
                cell.setCellValue(StringUtils.isEmpty(value) ? "" : value);
            }
            i++;
        }
        //写回文件
        write2Browser(response, workbook, fileName);


    }

    private static void write2Browser(HttpServletResponse response, Workbook wb, String fileName) {
        try {
            fileName = new String(fileName.getBytes(), "ISO8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            response.setHeader("content-type", "application/octet-stream");
            //设置Http响应头告诉浏览器下载这个附件
            response.setHeader("Content-Disposition", "attachment;Filename=" + fileName + System.currentTimeMillis() + XLSX);
            response.flushBuffer();
            OutputStream outputStream = response.getOutputStream();
            wb.write(outputStream);
            outputStream.close();
        } catch (Exception ex) {
            _logger.info(String.format("文件导出失败：%s", ex));
            ex.printStackTrace();
        }
    }
    private static void write2Path(String path, Workbook wb, String fileName) {
        ByteArrayOutputStream byteArrayOutputStream=null;
        FileOutputStream fileOutputStream =null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();//缓冲流
            wb.write(byteArrayOutputStream);
            File file = new File(path + File.separator + fileName+".xlsx");//File.separator创建系统分割符 Unix的'/'或者'\'
            if(!file.getParentFile().exists()){
                file.getParentFile().mkdirs();
            }
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                if(fileOutputStream!=null){
                    fileOutputStream.close();
                }
                if(byteArrayOutputStream!=null){
                    byteArrayOutputStream.close();
                }
                wb.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static <T> List<T> importExcel(Class<T> clazz, MultipartFile file) throws Exception {
        Workbook wk;
        List<T> list = new ArrayList<>();
        String fileName = file.getOriginalFilename();
        InputStream inputStream = file.getInputStream();
        //判断excel文件类型（2003 xls,2007 xlsx）
        if (fileName.endsWith(XLS)) {
            wk = new HSSFWorkbook(inputStream);
        } else if (fileName.endsWith(XLSX)) {
            wk = new XSSFWorkbook(inputStream);
        } else {
            throw new RuntimeException("非Excel文件,无法导入");
        }
        //获取第一个sheet页
        Sheet sheet = wk.getSheetAt(0);
        //获取总行数
        int rows = sheet.getPhysicalNumberOfRows();
        if (rows == 0) {
            throw new RuntimeException("文件为空!");
        }
        //获取总列数
        int cells = sheet.getRow(0).getPhysicalNumberOfCells();
        //获取实体类中的所有字段
        Field[] fields = clazz.getDeclaredFields();
        //从第二行开始
        for (int i = 1; i < rows; i++) {
            T t = clazz.newInstance();
            //声明一个初始位置
            int index = 0;
            Row row = sheet.getRow(i);
            if (row == null) {
                continue;
            }
            while (index <= cells) {
                Cell cell = row.getCell(index);
                if (cell == null) {
                    cell = row.createCell(index);
                }
                cell.setCellType(CellType.STRING);
                //实体类的顺序一定要和Excel中字段的顺序一致
                String value = cell.getStringCellValue() == null ? "" : cell.getStringCellValue();
                Field field = fields[index];
                //获取字段名
                String name = field.getName();
                //方法名
                String method = "set" + name.substring(0, 1).toUpperCase() + name.substring(1);
                //执行方法
                Method setMethod = clazz.getMethod(method, Object.class);
                //方法执行
                setMethod.invoke(t, value);
                index++;
            }
            if (checkBean(t)) {
                list.add(t);
            }
        }
        inputStream.close();
        _logger.info("导入的数据{}", list);
        return list;
    }

    private static <T> boolean checkBean(T t) {
        boolean flag = false;
        Field[] fields = t.getClass().getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);//可以访问private修饰的字段
                if (null != field.get(t)) {
                    flag = true;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return flag;
    }
}
