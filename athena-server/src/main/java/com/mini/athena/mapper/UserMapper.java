package com.mini.athena.mapper;

import com.mini.athena.domain.PermissionInfo;
import com.mini.athena.domain.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ClassName : UserMapper
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/3  16:35
 */
@Mapper
public interface UserMapper {

    UserInfo getUserByUserAccount(@Param("account") String account);

    List<PermissionInfo> getPermissionById(@Param("id") int userId);

    //账号注册
    void register(@Param("userInfo") UserInfo userInfo);
    //账号注销
    void cancel(@Param("username") String username);

    //修改用户信息
    void updateUser(@Param("userInfo")UserInfo userInfo);
    //修改密码
    void updatePassword(@Param("id") int id,@Param("newPassword") String newPassword);

}
