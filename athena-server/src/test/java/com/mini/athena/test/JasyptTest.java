package com.mini.athena.test;

import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName : JasyptTest
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/16  11:14
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class JasyptTest {

    @Autowired
    StringEncryptor encryptor;

    @Test
    public void EncryptPassword(){

        //加密
        String name = encryptor.encrypt("admin");
        String password = encryptor.encrypt("123456");
        System.out.println("name 密文: " + name);
        System.out.println("password 密文: " + password);

        //解密
        String decrypt1 = encryptor.decrypt(name);
        String decrypt2 = encryptor.decrypt(password);
        System.out.println(decrypt1 + "------------" + decrypt2);

    }

}
