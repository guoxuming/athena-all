package com.mini.athena.test;

import com.alibaba.druid.filter.config.ConfigTools;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * @ClassName : DruidTest
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/16  11:14
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DruidTest {

    @Test
    public void druidEncryptPassword() throws Exception {
        String password = "athena";
        System.out.println("明文密码: "+password);

        String[] keyPair = ConfigTools.genKeyPair(512);//定义秘钥长度

        String privateKey = keyPair[0];//秘钥
        String publicKey = keyPair[1];//公钥
        password = ConfigTools.encrypt(privateKey,password);
        System.out.println(password.length());
        System.out.println(privateKey.length());
        System.out.println(publicKey.length());
        System.out.println("privateKey:" + privateKey);
        System.out.println("publicKey:" + publicKey);
        System.out.println("加密密码: "+password);


        String decrypt = ConfigTools.decrypt(publicKey, password);
        System.out.println("解密后密码: " +decrypt);


    }
}
