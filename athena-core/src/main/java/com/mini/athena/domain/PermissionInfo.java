package com.mini.athena.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName : PermissionInfo
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/3  17:13
 */
@Data
@ApiModel("用户资源信息")
public class PermissionInfo {


    @ApiModelProperty("资源标识")
    private Integer id;
    @ApiModelProperty("资源名称")
    private String resourceName;
    @ApiModelProperty("资源图标")
    private String resourceIcon;
    @ApiModelProperty("资源路径")
    private String resourceHref;
    @ApiModelProperty("资源类型")
    private String resourceType;
    @ApiModelProperty("资源权限")
    private String resourceAuth;
    @ApiModelProperty("子资源")
    private List<PermissionInfo> child;

}
