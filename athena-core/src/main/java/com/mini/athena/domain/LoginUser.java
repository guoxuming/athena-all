package com.mini.athena.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mini.athena.enums.UserStatus;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName : LoginUser
 * @Description :  登录用户信息
 * @Author : Guoxuming
 * @Date : 2020/9/11  10:37
 */
public class LoginUser extends UserInfo implements UserDetails {


    private static final long serialVersionUID = -6537426276306616461L;

    //资源权限
    private List<PermissionInfo> permissionInfos;
    //token
    private String uuid;
    //登陆时间毫秒值
    private Long loginTime;
    //过期时间值
    private Long expireTime;


    @Override
    @JsonIgnore  //反序列化忽略此方法
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return permissionInfos.stream().filter(permissionInfo -> StringUtils.isNotEmpty(permissionInfo.getResourceAuth()))
                .map(permissionInfo -> new SimpleGrantedAuthority(permissionInfo.getResourceAuth())).collect(Collectors.toList());
    }

    //账户是否未过期
    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }
    //账户是否锁定
    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return !getAccountStatus().equals(UserStatus.LOCKED.getStatus());
    }

    //密码是否未过期
    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    //是否使用
    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }

    public List<PermissionInfo> getPermissionInfos() {
        return permissionInfos;
    }

    public void setPermissionInfos(List<PermissionInfo> permissionInfos) {
        this.permissionInfos = permissionInfos;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Long loginTime) {
        this.loginTime = loginTime;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    @Override
    public String toString() {
        return "LoginUser{" +
                "permissionInfos=" + permissionInfos +
                ", uuid='" + uuid + '\'' +
                ", loginTime=" + loginTime +
                ", expireTime=" + expireTime +
                "} " + super.toString();
    }
}
