package com.mini.athena.domain;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @ClassName : Token
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/8  14:57
 */
@Data
@ToString
public class Token implements Serializable {


    private static final long serialVersionUID = 37828831246156229L;
    //令牌信息
    private String token;
    //登陆时间  毫秒值
    private Long loginTime;

    public Token() {
    }

    public Token(String token, Long loginTime) {
        this.token = token;
        this.loginTime = loginTime;
    }


}
