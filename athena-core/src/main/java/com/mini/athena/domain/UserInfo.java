package com.mini.athena.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName : UserInfo
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/3  15:51
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("账户信息")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 4430910124423117493L;
    @ApiModelProperty("标识")
    private int id;
    @ApiModelProperty("账户")
    private String username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("姓名")
    private String userTitle;
    @ApiModelProperty("电话")
    private String userPhone;
    @ApiModelProperty("生日")
    private String birthday;
    @ApiModelProperty("身份证")
    private String idCard;
    @ApiModelProperty("地址")
    private String userAddr;
    @ApiModelProperty("性别")
    private String sex;
    @ApiModelProperty("账户状态")
    private String accountStatus;
    @ApiModelProperty("登录失败次数")
    private String failCount;
    @ApiModelProperty("锁定时间")
    private String lockTime;
    @ApiModelProperty("创建时间")
    private String insertTime;
    @ApiModelProperty("更新时间")
    private String updateTime;



}
