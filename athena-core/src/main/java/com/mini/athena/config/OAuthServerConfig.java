package com.mini.athena.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

/**
 * @ClassName : OAuthServerConfig
 * @Description :  认证服务器
 * @Author : Guoxuming
 * @Date : 2020/9/9  16:41
 */
//@Deprecated
@Configuration
@EnableAuthorizationServer//表示认证服务器
public class OAuthServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    //    配置授权模式    授权码或者密码模式

        clients.inMemory().withClient("clientId")//标识客户端的
                 .secret(passwordEncoder.encode("123"))//可以类比为密码
        .authorizedGrantTypes("authorization_code")//授权码授权
        .scopes("all")//作用范围
        .accessTokenValiditySeconds(1800);//过期时间
    }


}
