package com.mini.athena.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @ClassName : WebSecurityConfig
 * @Description : security配置
 * @Author : Guoxuming
 * @Date : 2020/9/4  10:30
 */
@Configuration
@EnableWebSecurity //1: 加载了WebSecurityConfiguration配置类, 配置安全认证策略。2: 加载了AuthenticationConfiguration, 配置了认证信息
@EnableGlobalMethodSecurity(prePostEnabled = true)//要想开启注解，需要在继承WebSecurityConfigurerAdapter的类上加@EnableGlobalMethodSecurity注解，prePostEnabled = true开启对@PreAuthorize("hasAuthority('')")的支持    来判断用户对某个控制层的方法是否具有访问权限
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);

    @Autowired
    private UserDetailsService userDetailsService;
    /**
      * @Description :  密码加密
      */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        //配置内存用户
        //auth.inMemoryAuthentication().withUser("zhangsan").password("123456").roles("ADMIN");
        //配置一些其他的限制 安全回话数校验,ip校验等
        //auth.authenticationProvider()
        //配置数据库用户信息
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }
    @Override
    public void configure(HttpSecurity http) throws Exception{

        //post请求默认的都开启了csrf的模式，所有post请求都必须带有token之类的验证信息才可以进入登陆页面，这边是禁用csrf模式
        http       //禁用basic和form认证 自己实现登陆登出
                .httpBasic().disable()
                .formLogin().disable()
                .csrf().disable()
                .logout().disable();

        //匹配/hello，且http method是POST，需要权限认证
        //http.authorizeRequests().antMatchers(HttpMethod.POST, "/v1/world").hasRole("USER");
        //对前端样式放行
        //http.authorizeRequests().antMatchers("/**/*.png").access("permitAll");
        //指上面没有匹配到的url都需要认证    不论权限是什么
        //http.authorizeRequests().anyRequest().authenticated();
        //等同于上面
        //http.authorizeRequests().antMatchers("/**/*.css","/**/*.js","/**/*.png","/login").permitAll().anyRequest().authenticated();
        //开放所有路径
        http.authorizeRequests().antMatchers("/", "/*.html", "/favicon.ico", "/css/**", "/js/**", "/fonts/**",  "/img/**",
                "/v2/api-docs-ext/**", "/v2/api-docs/**","/swagger-resources/**", "/webjars/**", "/pages/**", "/druid/**",
                "/statics/**","/login","/openApi/**","/verificationCode","/logout","/checkCode/**").permitAll().anyRequest().authenticated();

        // 解决不允许显示在iframe的问题
        http.headers().frameOptions().disable();
        //
        http.headers().cacheControl();
    }
    @Override
    @Bean   //注入认证管理器 AuthenticationManager  能够自己实现
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

}
