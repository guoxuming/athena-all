package com.mini.athena.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName : WebMvcConfig
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/9/2  16:56
 */
//@Configuration
@Deprecated
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void  addCorsMappings(CorsRegistry registry){
        //允许跨域
        registry.addMapping("/**").allowedHeaders("*")
                .allowedMethods("*").allowedOrigins("*");
    }

}
