package com.mini.athena.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ClassName : Swagger2Config
 * @Description : swagger 配置类
 * @Author : Guoxuming
 * @Date : 2020/7/10  14:44
 */
@Configuration
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
public class Swagger2Config {

    //swagger的配置信息
    //@Bean
    //public Docket createRestApi(){
    //
    //    return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
    //            //.apis(RequestHandlerSelectors.any())
    //            .apis(RequestHandlerSelectors.basePackage("com.tl.security.controller"))
    //            .paths(PathSelectors.any()).build();
    //}

    //knife4j的配置信息
    @Bean
    public Docket defaultApi2(){

        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
                //分组名称
                .groupName("2.X版本")
                .select()
                //指定controller路径 basePackage("com.mini.athena.api.controller")
                .apis(RequestHandlerSelectors.basePackage("com.mini.athena.api.controller"))
                .paths(PathSelectors.any())
                .build();
    }
    //采用默认的参数
    //private ApiInfo apiInfo(){
    //    return ApiInfo.DEFAULT;
    //}
    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("Athena接口测试")
                .description("多参数接口测试")
                .contact(new Contact("miniming","www.miniming.com",""))
                .version("1.0.0.SNAPSHOT")
                .build();
    }
}
