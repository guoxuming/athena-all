package com.mini.athena.filter;

import com.mini.athena.domain.LoginUser;
import com.mini.athena.service.JwtTokenService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName : TokenFilter
 * @Description :  一次请求只过一次的过滤器    且需要重复执行
 * @Author : Guoxuming
 * @Date : 2020/9/3  17:44
 */
@Component
public class JwtTokenFilter extends OncePerRequestFilter {


    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private UserDetailsService userDetailsService;


    private static final String TOKEN_KEY = "token";

    private static final Long MINUTES_10 = 10 * 60 * 1000L;

    @Override
    protected void doFilterInternal(
            HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String token = getToken(httpServletRequest);
        if (StringUtils.isNoneBlank(token)) {
            LoginUser loginUser = jwtTokenService.getUserInfo(token);
            if (loginUser != null) {
                loginUser = checkUserPermission(loginUser);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
                //容器中设置用户信息
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            } else {
                throw new RuntimeException("无效的token信息，请重新登录！");
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
        // 清除用户信息
        SecurityContextHolder.clearContext();
    }

    //从请求头获取token信息
    private String getToken(HttpServletRequest req) {
        //从parameter中获取token   如果不存在就在header中获取token
        String token = req.getParameter(TOKEN_KEY);
        if (StringUtils.isBlank(token)) {
            token = req.getHeader(TOKEN_KEY);
        }
        return token;
    }

    private LoginUser checkUserPermission(LoginUser loginUser) {
        //过期时间
        Long expireTime = loginUser.getExpireTime();
        //当前时间
        long currentTime = System.currentTimeMillis();
        //每次访问都需要刷新缓存    对redis操作太过于频繁    改成为与过期时间相差十分钟刷新缓存
        if (expireTime - currentTime <= MINUTES_10) {
            String uuid = loginUser.getUuid();
            //刷新用户信息及权限
            loginUser = (LoginUser) userDetailsService.loadUserByUsername(loginUser.getUsername());
            loginUser.setUuid(uuid);
            //刷新缓存信息
            jwtTokenService.refreshToken(loginUser);
        }
        return loginUser;
    }
}
