package com.mini.athena.enums;

/**
 * @ClassName : UserStatus
 * @Description : 用户账户状态
 * @Author : Guoxuming
 * @Date : 2020/9/10  15:47
 */
public enum UserStatus {
    //账号锁定
    LOCKED ("2"),

    DISABLE("3");

    private String status;

    UserStatus() {
    }

    UserStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UserStatus{" +
                "status='" + status + '\'' +
                "} " + super.toString();
    }
}
