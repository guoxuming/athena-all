package com.mini.athena.service.impl;

import com.mini.athena.domain.LoginUser;
import com.mini.athena.domain.Token;
import com.mini.athena.service.JwtTokenService;
import com.mini.athena.utils.UUIDAndDateutil;
import io.jsonwebtoken.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * @ClassName : JwtTokenServiceImpl
 * @Description : jwt令牌的刷新   生成等
 * @Author : Guoxuming
 * @Date : 2020/9/11  10:59
 */
@Service
public class JwtTokenServiceImpl implements JwtTokenService {

    private Logger _logger = LoggerFactory.getLogger(JwtTokenServiceImpl.class);

    @Autowired
    RedisTemplate redisTemplate;

    //token过期时间
    @Value("${token.expireSeconds}")
    private int expireSeconds;
    //自定义随机盐
    @Value("${token.jwtSecret}")
    private String jwtSecret;

    private volatile static Key KEY = null;//随机盐
    private static final String LOGIN_USER_KEY = "LOGIN_USER_KEY";//获取token的key
    private static final String LOGIN_USER_NAME = "sub"; //获取账号的key

    @Override
    public Token saveToken(LoginUser loginUser) {
        //设置tokenkey
        loginUser.setUuid(UUIDAndDateutil.getUUID());
        //缓存用户信息
        storageLoginUser(loginUser);
        //生成jwt令牌
        String jwtToken = createJWTToken(loginUser);

        return new Token(jwtToken, loginUser.getLoginTime());
    }

    @Override
    public LoginUser getUserInfo(String jwtToken) {
        String uuid = getUUIDFromJwtToken(jwtToken);
        if (uuid != null) {
            LoginUser loginUser = (LoginUser) redisTemplate.opsForValue().get(getRedisKey(uuid));
            return loginUser;
        }
        return null;
    }

    @Override
    public String getUsername(String jwtToken) {
        try {
            Claims claims = getClaimsFromJwtToken(jwtToken);
            if (claims != null) {
                return claims.getSubject();
            }
            return null;
        } catch (Exception e) {
            _logger.error("{}", e.getMessage());
            return null;
        }
    }

    @Override
    public void refreshToken(LoginUser loginUser) {
        storageLoginUser(loginUser);
    }

    @Override
    public boolean delToken(String jwtToken) {
        String uuid = getUUIDFromJwtToken(jwtToken);
        if (uuid == null) {
            return false;
        }
        String redisKey = getRedisKey(uuid);
        LoginUser loginUser = (LoginUser) redisTemplate.opsForValue().get(redisKey);//日志操作使用
        redisTemplate.delete(redisKey);//退出操作删除缓存信息
        return true;
    }

    private String getUUIDFromJwtToken(String jwtToken) {
        try {
            Claims claims = getClaimsFromJwtToken(jwtToken);
            if (claims != null) {
                return claims.get(LOGIN_USER_KEY).toString();
            }
            return null;
        } catch (Exception e) {
            _logger.error(e.getMessage());
            return null;
        }
    }


    private Claims getClaimsFromJwtToken(String jwtToken) {
        if (jwtToken == null || StringUtils.isBlank(jwtToken)) {
            return null;
        }
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(getInstanceKey()).parseClaimsJws(jwtToken).getBody();
        } catch (Exception e) {
            claims = null;
            _logger.error(e.getMessage());
        }
        return claims;
    }

    //用户信息缓存
    private void storageLoginUser(LoginUser loginUser) {
        //设置登陆时间
        loginUser.setLoginTime(System.currentTimeMillis());
        loginUser.setExpireTime(loginUser.getLoginTime() + expireSeconds * 1000L);
        //redisTemplate.boundValueOps(getRedisKey(loginUser.getUuid())).set(loginUser, expireSeconds, TimeUnit.SECONDS);
        String redisKey = getRedisKey(loginUser.getUuid());
        redisTemplate.opsForValue().set(redisKey, loginUser);
        redisTemplate.expire(redisKey, expireSeconds, TimeUnit.SECONDS);
        //剩余时间
        Long expire = redisTemplate.getExpire(redisKey, TimeUnit.SECONDS);
        _logger.info("redis缓存信息剩余时间:{}s", expire);
    }

    private String getRedisKey(String uuid) {
        return "token:" + uuid;
    }


    private String createJWTToken(LoginUser loginUser) {
        Map<String, Object> map = new HashMap<>();
        map.put(LOGIN_USER_NAME, loginUser.getUsername());
        map.put(LOGIN_USER_KEY, loginUser.getUuid());
        return Jwts.builder().setClaims(map).signWith(SignatureAlgorithm.HS256, getInstanceKey()).compact();//添加一个随机盐

    }

    //自定义秘钥 转码加密
    private Key getInstanceKey() {
        if (KEY == null) {
            synchronized (JwtTokenServiceImpl.class) {
                if (KEY == null) { //DCL
                    byte[] key = DatatypeConverter.parseBase64Binary(jwtSecret);
                    KEY = new SecretKeySpec(key, SignatureAlgorithm.HS256.getJcaName());
                }
            }
        }
        return KEY;
    }

}
