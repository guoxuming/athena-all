package com.mini.athena.service;

import com.mini.athena.domain.LoginUser;
import com.mini.athena.domain.Token;

/**
 * @ClassName : JwtTokenService
 * @Description :   tokeng管理器  可以存储到redis或者数据库
 * @Author : Guoxuming
 * @Date : 2020/9/7  18:30
 */
public interface JwtTokenService {
    //保存用户信息到redis
    Token saveToken(LoginUser loginUser);
    //根据令牌查询用户信息
    LoginUser getUserInfo(String jwtToken);
    //根据令牌查询用户姓名
    String getUsername(String jwtToken);
    //刷新redis时间
    void refreshToken(LoginUser loginUser);
    //删除redis缓存信息
    boolean delToken(String jwtToken);


}
