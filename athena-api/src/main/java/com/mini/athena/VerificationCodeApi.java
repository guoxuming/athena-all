package com.mini.athena;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.Map;

/**
 * @ClassName : api
 * @Description :
 * @Author : Guoxuming
 * @Date : 2020/8/28  18:45
 */
@FeignClient(name = "athena-server" ,path = "/athena")
public interface VerificationCodeApi {

    @RequestMapping(value = "/openApi/getCodeOfBase64" ,method = RequestMethod.GET)
    Map<String,String> getCodeOfBase64() throws IOException;
}
